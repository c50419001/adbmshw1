#!/bin/bash
# bash command-line arguments are accessible as $0 (the bash script), $1, etc.
# echo "Running" $0 "on" $1
#echo "Replace the contents of this file with your solution."
######################################################### Hw 1 ########################################################################
#							Prepare
########################################################################################################################################
#
#	Step.1 Writing the header row to each file, including  ebook.csv, tokens.csv, token_counts.csv, name_counts.csv
#
########################################################################################################################################
#							Part. 1
########################################################################################################################################
#
#	Step.1 Reading the source file the user give, and take the part we want.
#		1-1. Giving garbage ( replacing space with X^^X ) to books' body (from START flag to END flag) in order to
#			recognize the difference KEYWORD between information rows and body.
#		1-2. Taking information rows.
#	Step.2 Deal with  the  non-author e-book.
#		2-1. Appending a QQQnullQQQ\r line after Title information line.
#		2-2. Concatenating QQQnullQQQ line and it's next line together.
#		2-3. Checking if non-author with the following keyword [Release Date:/Author:]
#			[Release Date:] => It means no author. Replacing "QQQnullQQQ,Release Date:" with "Author: null\r\nRelease Date:"
#			[Author:]		=> It means the e-book have a known author. Replacing "QQQnullQQQ,Author:" with "Author:"
#	Step.3 Let the contents fit format of csv.
#		3-1. change " into  "".
#		3-2. Finding the information rows, replacing the real ',' and '"' with garbage "X%%%%%%X" and "XOOOOOOX" for next action.
#		3-3. Giving the comma following each information to separate them. ( Now, we just have the comma for separating,
#			not the old ones replacing with X%%%%%%X. )
#			[Title: /Author: ]	=> Adding ',' after each raw.
#			[Release date: ]	=> Separating the raw into two part: Release date info and e-book number with 
#				keyword "ebook" or "etext".
#				note. Both "ebook" and "etext"  may be different capitalization. (Ebook, eBook, Etext, etext......)
#			[Language: ]		=> Adding extra '"' after adding the ',' following the raw for the beginning of
#				body column.
#	Step.4 Recovering the changed symbol for information raws and body.
#		4-1. Recover "X%%%%%%X" back to ','	(info)
#		4-2. Recover "XOOOOOOX" back to '"' (info)
#		4-3. Recover "X\^\^X" back to '"' (body)
#	Step.5 Deleting the flag raws of bodies.
#		5-1. Deleting the starting flag raw and it's next line by "\*\*\* *START OF THE PROJECT.*\*\*\*"
#		5-2. Replacing the ending flag raw with '"' to close the body block.
#	Step.6 Passing the data stream to tee to append the result to ebook.csv and being the input stream of part. 2
#
#########################################################################################################################################
#							Part. 2
#########################################################################################################################################
#
#	Step.1 Transform Upper case into lower case.
#	Step.2 Replacing "" with garbage XXKKKXX
#	Step.3 Printing information raws completely and bodies without number and symbol.
#		3-1. using p to print info raws, and deleting them from pattern space.
#		3-2. Replacing all the symbol and number with space (separating into tokens ) and print them ( Bodies in 
#			pattern space separate into tokens ).
#	Step.4 Removing " symbol which means the ending flag.
#	Step.5 leaving only the book's number in information raws. (5-1...5-X is in information raws )
#		5-1. In info raws, removing the words surround by '"', ex. "words words".
#		5-2. In info raws, removing the words including dash sign (-) and it maybe with numbers.
#		5-3. In info raws, removing non-number characters.
#	Step.6 Formating
#		6-1. Assigning tokens to every line that one line contains exactly one token.
#		6-2. Outputting the line containing things. ( book number or token )
#		6-3. Using hold space to store the book number, and printing it after the token raw. (x; is exchanging the
#			container of pattern space and hold space.)
#		6-4. Concatenating book number raw and token raw together. Replace the \n between two raws with ',', and
#			appending '\r' after the concatenated raw.
#	Step.7 Passing the data stream to tee to append the result to tokens.csv and being the input stream of part. 3
#
##########################################################################################################################################
#							Part. 3
##########################################################################################################################################
#
#	Step.1 Taking the data we want.
#		1-1. Dealing with "\r". We must remove it to prevent the judging error by awk.
#		1-2. Using awk print the second column of the input. ( tokens )
#	Step.2 Counting how many times each token appeared.
#		2-1. Sorting the token, using command uniq -c to help us getting how many times each token appeared. (We have to sort 
#			data before using uniq command. )
#		2-2. exchanging the token column and counting column make the raws fit the format we want.
#	Step.3 Passing the data stream to tee to append the result to token_counts.csv and being the input stream of part. 4
#
###########################################################################################################################################
#							Part. 4
###########################################################################################################################################
#
#	Step.1 Using grep get the token in the file "popular_names.txt". (-i,ignoring the case. -w, selecting only those lines 
#		containing matches that form whole words. -f, obtaining patterns from FILE, one per line.)
#	Step.2 Redirecting the standard output to the file "name_counts.csv"
#
############################################################################################################################################
echo -e "title,author,release_date,ebook_id,language,body\r" > ebook.csv;
echo -e "ebook_id,token\r" > tokens.csv;
echo -e "token,count\r" > token_counts.csv;
echo -e "token,count\r" > name_counts.csv;

cat ${1} |\
	sed -n '/\*\*\* START OF THE PROJECT /,/\*\*\* END OF THE PROJECT /{s/ /X^^X/g;p};/^Title: \|^Author: \|^Release Date: \|^Language: /p'\
	| sed '/Title: /a QQQnullQQQ\r'\
	| sed '/QQQnullQQQ/{N;s/\r\n/,/};'\
	| sed 's/QQQnullQQQ,Release Date:/Author: null\r\nRelease Date:/;s/QQQnullQQQ,Author:/Author:/'\
	| sed 's/"/""/g'\
	| sed '/Title: \|Author: \|Release Date: \|Language: /{s/,/X%%%%%%X/g;s/""/XOOOOOOX/g}'\
	| sed '/Title: \|Author: /{s/\r/,\r/};/Release Date: /{s/ *\[[Ee][Bb]ook #\| *\[[Ee][Tt]ext #/,/;s/\]/,/};/Language: /{s/\r/,"\r/}'\
	| sed '/Title: \|Author: \|Release Date: \|Language: /{ /X%%%%%%X\|XOOOOOOX/{s/^/"/;s/,/",/}};'\
	| sed '/Title: /{N;N;N;s/\r\n//g};s/Title: //;s/Author:  *//;s/Release Date: //;s/Language: //'\
	| sed 's/X%%%%%%X/,/g;s/XOOOOOOX/""/g;s/X\^\^X/ /g;'\
	| sed '/\*\*\* *START OF THE PROJECT.*\*\*\*/{N;d}'\
	| sed 's/\*\*\* *END OF THE PROJECT.*\*\*\*/"/'\
	| tee -a ebook.csv | tr 'A-Z' 'a-z' \
	| sed 's/""/XXKKKXX/g;' \
	| sed -n '/"/{p;d};s/[^a-z]/ /g;p' \
	| sed '/^"\r/d;' \
	| sed '/"/{s/"[^"]*"//g;s/[0-9]* *[-a-z] *[0-9]*//g;s/[^0-9]//g}' \
	| sed 's/\s\s*/\n/g;' \
	| sed -n '/[0-9a-z]/p' \
	| sed -n '/[0-9]/h;/[a-zA-Z]/{x;p;x;p}' | sed 'N;s/\n/,/;s/$/\r/' \
	| tee -a  tokens.csv | sed 's/\r//' | awk -F, '{print($2)}' \
	| sort | uniq -c | awk '{print($2,$1)}' | sed 's/ /,/;s/$/\r/' \
	| tee -a  token_counts.csv | grep -iwf popular_names.txt >> name_counts.csv;
exit 0
